// DERNIERE CONFIG: ARROSAGE03 du CLC avec Domoticz local

/********* Control Arrosage Intelligent *********
 *  
 *  
 *  VERSION DIFFUSEE en Juillet 2002 en 7 exemplaires. NE PAS MODIFIER !!!!!!!!!!!!!!
 *  
   par Stephane et Sylvain
   v0.1 Mesure et comparaison au seuil entré. Activation electro/pompe. Aucun filtrage.
        Par défaut Mode 4
   v0.2 Stock en EEPROM les réglages Seuil et Mode après une durée sans changement (write blinks 3 if OK, 6 if NOK)
        Récupere les valeurs en EEprom au reset
        Pas de delay dans la boucle (sauf 10ms après analogRead et sleep)
        Si humide, attente  SLEEPDURATION min sans mesure (sleep blinks 1 long 2 short). Si sec, arroser selon le cycle.
        Fait tourner le moteur 1 sec avant le sleep pour prévenir le blocage
        Duree max arrosage, puis attente plusieurs minutes (diffusion de l'eau). C'est le mode cycle.
        DEMOMODE=60 applique au cycle des minutes au lieu de secondes
   v0.3 BATTERYMODE: Sleep mode avec cablage RST - D0
        BUTTONWIFI: Wifi mode, avec desactivation du sleep mode, et server wifi toujours actif
        Mode wifi: blink toutes les 5 secondes
        Wifi mode timeout: désactiver le wifi si aucune action durant 10min
   v0.4 OFFCIELLE DIFFUSEE
   v0.5 Ajout envoi valeur à DOMOTICZ
   A tester:
      Arrosage timeout: couper si vraiment très long.
   Non fait:
      ? Changer : si sec passer aussi en mode veille selon les cycles
      ? Filtrage des mesures
      Ajouter le param DEMOMODE à la GUI
      Envoi stats à Domoticz ou Graphana

**********************************************/

// ********* PARAMETRES REGLABLES ****************
// (duree en seconds)
#define DUREEMAXARROSAGE 120
#define PAUSEARROSAGE    300
#define SEUILHUMIDDEFAUT 20

// DEMOMODE : in Demo and Test modes reduce delays to get quicker visible transitions
//    DEMOMODE 3    for TEST/DEMO (=1 second for quicker)
//    DEMOMODE 100  for PROD (=100 seconds is a factor applied to delays. Any factor other than 1 is possible)
#define DEMOMODE 60

// 300000 =  5 minutes
// 900000 = 15 minutes
#define SLEEPDURATION 900000

// Maximum uptime even if watering or wifi up. And NOT BATTERYMODE.
// 1.200.000 = 20 min
#define MAXUPTIME 1200000

// During sleep, millisec() is not reset and still runs.
unsigned long MaxUptime = millis()+MAXUPTIME;

// BATTERYMODE enables deep sleep instead of delay. Connect D0 to RST.
#define BATTERYMODE
// wifimode is true if BUTTONWIFI was pushed after just after startup
bool wifimode = false;
// Push stats to Internet. Requires a wifi network connected to Internet.
#define DATA2INTERNET
// ******** Fin de PARAMETRES REGLABLES ****************  

// ********* CONFIGURATION WIFI ****************
// This is a local P2P wifi with a phone. No connection to Internet.
const char* ssid = "Arrosage03"; //Replace with your own SSID
const char* password = "123456789"; //Replace with your own password
// ********* Fin de CONFIGURATION WIFI ****************

// ********* CONFIGURATION HARDWARE ****************
// /* config D1 Mini
#define LEDPIN      D4       // D4 on Lolin v3, 16 on some others
#define SENSORPIN   A0
#define SENSORPOWER D7
#define BUTTONWIFI  D8
#define ELECTROPIN  D5
// */

/* config ESP de Stephane
#define LEDPIN 16       // D4 on Lolin v3, 16 on some others
#define SENSORPOWER D7
#define BUTTONWIFI D8
#define ELECTROPIN 0
*/
// ********* Fin de CONFIGURATION HARDWARE ****************

// Import des bibliothèques
#include <FS.h>
#include <LittleFS.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

// Variables
const char* PARAM_INPUT = "value";
String sliderValue = "0";
int sondeMeasure = 0;
int SeuilHumidite = SEUILHUMIDDEFAUT;
int ModeDuree = 4;
bool water = false;
unsigned long TimeToChange = 0;
unsigned long TimeUp = 0;
unsigned long TimeStop = 0;
//const int LEDPIN = 16;
//const int ELECTROPIN = 0;
String ledState;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// **************** EEPROM Storage *******************
// Store the values set in web interface
// include EEPROM Library
#include<EEPROM.h>
/* Defining data storage addresses
   We use 2 bytes. Values between 0 and 254*/
int eepromAddrThreshold = 0;
int eepromAddrMode = 1;
unsigned long TimeToWriteEeprom = 0;
// delay in millisec : 3 min
#define DelayBeforeWriteEeprom 180000
// **************** Fin de EEPROM Storage *******************

// **************** SEND DATA TO INTERNET SERVER *******************
#ifdef DATA2INTERNET
#include "Data2Internet.h"

// Chab P01
// #define INTERNETDEVICE 34
// Mesnilab CLC Arrosage03
#define INTERNETDEVICE 1

#endif
// **************** End of SEND DATA TO INTERNET SERVER *******************

String iw_SondeMeasure() {
  return String(sondeMeasure);
  }
// Added v4 Steph
String iw_ModeDuree() {
  //Serial.println(ModeDuree);
  return String(ModeDuree);
  }
String iw_Threshold() {
  //Serial.println(SeuilHumidite);
  return String(SeuilHumidite);
  }

String processor(const String& var){
  //Serial.println(var);
  if (var == "SLIDERVALUE"){
    return sliderValue;
  }
  return String();
    Serial.println(String());
}

void blink(int PIN, int count = 3, int duree = 300)
   {
    digitalWrite(PIN, HIGH);  // LED off
    for (int i=0 ; i < count ; i++ )
       {
       delay(duree);
       digitalWrite(PIN, LOW);   // LED on
       delay(duree);
       digitalWrite(PIN, HIGH);  // LED off
       }

   }

void setup(){

  pinMode(BUTTONWIFI, INPUT);  // D8 MUST be LOW (unconnected on boot!)
  pinMode(LEDPIN, OUTPUT);
  pinMode(ELECTROPIN, OUTPUT);  
  pinMode(SENSORPOWER, OUTPUT);

  digitalWrite(SENSORPOWER, LOW);
  digitalWrite(ELECTROPIN, LOW);

  Serial.begin(115200);
  delay(2000);  // Mandatory or expect crash  later on print
  Serial.println("Starting");
  Serial.println("Version 080622");
  Serial.print("DEMOMODE: "); Serial.println(DEMOMODE);
//delay(2000);
  // ********* Recover Threshold and Mode values if any
  /* Begin with EEPROM by deciding how much EEPROM memory you want to use.
     The ESP8266's maximum EEPROM size is 4096 bytes (4 KB), but we're just using 2 bytes here.
  */
  EEPROM.begin(2);
  delay(50);
  SeuilHumidite  = (int)EEPROM.read(eepromAddrThreshold);
  Serial.print("Eeprom Threshold:"); Serial.print(SeuilHumidite);
  ModeDuree      = (int)EEPROM.read(eepromAddrMode);
  Serial.print(" Mode:"); Serial.println(ModeDuree);
  if ((SeuilHumidite > 100) || (SeuilHumidite < 0)) {SeuilHumidite = SEUILHUMIDDEFAUT;}
  if ((ModeDuree > 4) || (ModeDuree < 0)) {ModeDuree = 4;}
  // ********* End of Recover Threshold and Mode values if any

  SetModeDelays();

// With ECO MODE, enable wifi and server only if Wifi button is pushed.
#ifdef BATTERYMODE
  Serial.println("Ecomode ON. Testing Wifi button during 3 sec.");
  byte i;
  for (i=0; i<15 ; i++)
    { 
      digitalWrite(LEDPIN, LOW);
      delay(100);
      digitalWrite(LEDPIN, HIGH);
      delay(100);
      if (digitalRead(BUTTONWIFI) == HIGH) { wifimode=true; i=50; }
    }
  if (wifimode)
  {
    Serial.println("WIFI Button ON. ");
#endif
    // Initialize LittleFS
    if(!LittleFS.begin()){
      Serial.println("An Error has occurred while mounting LittleFS");
      return;
    }
  
    // Connect to Wi-Fi
    Serial.print("Setting AP (Access Point)…");
    // Remove the password parameter, if you want the AP (Access Point) to be open
    WiFi.softAP(ssid, password);
  
    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP);
  
    // Route for root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(LittleFS, "/index.html", String(), false, processor);
    });

    // Route for guide page
    server.on("/guide.html", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(LittleFS, "/guide.html", String(), false, processor);
    });
    
    // Route to load style.css file
    server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(LittleFS, "/style.css", "text/css");
    });
  
    // Send a GET request to <ESP_IP>/slider?value=<inputMessage>
    server.on("/slider", HTTP_GET, [] (AsyncWebServerRequest *request) {
      String inputMessage;
      // GET input1 value on <ESP_IP>/slider?value=<inputMessage>
      if (request->hasParam(PARAM_INPUT)) {
           inputMessage = request->getParam(PARAM_INPUT)->value();
           // sliderValue = inputMessage;
           SeuilHumidite = inputMessage.toInt();
           TimeToWriteEeprom = millis() + DelayBeforeWriteEeprom;
           }
      else {
           inputMessage = "No message sent";
           }
      Serial.println("Seuil humidité: " + inputMessage);
      request->send(200, "text/plain", "OK");
    });
  
    // Send a GET request to <ESP_IP>/duree?value=<inputMessage>
    server.on("/duree", HTTP_GET, [] (AsyncWebServerRequest *request) {
      String inputMessage;
      // GET input1 value on <ESP_IP>/slider?value=<inputMessage>
      if (request->hasParam(PARAM_INPUT)) {
           inputMessage = request->getParam(PARAM_INPUT)->value();
           // sliderValue = inputMessage;
           ModeDuree = inputMessage.toInt();
           TimeToChange = millis();
           TimeToWriteEeprom = millis() + DelayBeforeWriteEeprom;
           // ********* Set mode delays
           SetModeDelays();
           }
      else {
           inputMessage = "No message sent";
           }
      Serial.println("Durée du cycle: " + inputMessage);
      request->send(200, "text/plain", "OK");
    });
  
    /*****************************************/
    server.on("/niv_batterie", HTTP_GET, [](AsyncWebServerRequest *request){
      // request->send_P(200, "text/plain", readDHTHumidity().c_str());
      request->send_P(200, "text/plain", iw_SondeMeasure().c_str());
    });

    // Added v4 Steph
	/*****************************************/
	server.on("/niv_hygrom", HTTP_GET, [](AsyncWebServerRequest *request){
	 // Mesure
	 request->send_P(200, "text/plain", iw_SondeMeasure().c_str());
	});

	server.on("/mode_duree", HTTP_GET, [](AsyncWebServerRequest *request){
	 // ModeDuree
	 request->send_P(200, "text/plain", iw_ModeDuree().c_str());
	});

  server.on("/threshold_hygrom", HTTP_GET, [](AsyncWebServerRequest *request){
   // ModeDuree
   request->send_P(200, "text/plain", iw_Threshold().c_str());
  });
  
    // Start server
    server.begin();
#ifdef BATTERYMODE
  }
  else
  {
  Serial.println("WIFI Button OFF. ");


  
  }   // End of BUTTONWIFI test
#endif

}  // end of setup
 
void loop(){
  unsigned long now=millis();
  
  // Update EEPROM if param has changed some time ago, and not recently
  if (now > TimeToWriteEeprom)
    {
    if (SeuilHumidite  != (int)EEPROM.read(eepromAddrThreshold))
       {
        Serial.print("Storing Threshold...");
        EEPROM.write(eepromAddrThreshold, (byte)SeuilHumidite); 
        eeprom_commit();
       }
    if (ModeDuree      != (int)EEPROM.read(eepromAddrMode))
       {
        Serial.print("Storing Mode...");
        EEPROM.write(eepromAddrMode,      (byte)ModeDuree);
        eeprom_commit();
       }
    TimeToWriteEeprom = now + DelayBeforeWriteEeprom;
    }


  // ********** Make a measure
  digitalWrite(SENSORPOWER, HIGH);
  // scale to sliderValue range  
  sondeMeasure = analogRead(SENSORPIN) * 100 / 1024;
  // to do: add filtering
  delay(10);  // Serial.print(String(sondeMeasure));Serial.println("=sonde");  

  
  digitalWrite(SENSORPOWER, LOW);

  // ********* Make decision
  //int sliderThreshold = sliderValue.toInt();
  if (sondeMeasure < SeuilHumidite)
     {
      // dry, let's water. But apply mode delays.
      if (now > TimeToChange) 
         {
          if (water) 
             {
              water = false;
              TimeToChange = now + TimeStop;
             }
          else
             {
              water = true;
              TimeToChange = now + TimeUp;
             }
         }
     }
   else 
      {
      // wet enough
      water = false;
      TimeToChange = now;
      watering(false);
      // wet: no action during SLEEPDURATION.
      if (wifimode)
        {
         // Just delay so that server still up
         // When WIFI up, blink shortly every 5 sec
         delay(5000);
         blink(LEDPIN, 1, 100);
        }
      else
        {
        Serial.println("Sleep now");
        // If BATTERYMODE going to deep sleep. Stops server as well.
        // But before sleeping, run motor to prevent stuck.
        watering(true);
        // delay(1000) is too much. Cause real watering.
        delay(200);
        goingtosleep();
/*
        watering(false);
        blink(LEDPIN, 2);
#ifdef BATTERYMODE
        Serial.println("Deep Sleep now");
        // Deep sleep test, trig D0 connected to RST after the delay
        // microsec
        // 1e6 = 1.000.000 => 1 sec !
        ESP.deepSleep(SLEEPDURATION * 1e3);
#else
        delay(SLEEPDURATION);
#endif
*/
        }
      }

   // ***** Do it
   watering(water);

#ifdef BATTERYMODE
   // Force deep sleep after delay whatever happen
   if (millis() > MaxUptime) 
      {
        Serial.print("Max time reached...");
        goingtosleep();
      }
#endif

}
// End of Loop

// 
void goingtosleep() {
        watering(false);
        blink(LEDPIN, 2);

#ifdef BATTERYMODE
#ifdef DATA2INTERNET
        // Before sleep, send measure via internet
        if (init_internet()) {
          Serial.println("Internet connected.");
          // Soil moisture (cb : 0 = wet,100 = dry, 200 = very dry)
          sendMeasure(INTERNETDEVICE, 100 - sondeMeasure); 
          Serial.print("Sent Internet "); Serial.println(100 - sondeMeasure);
        }
        else {
          Serial.println("Internet failed.");
        }
#endif
        Serial.println("Deep Sleep now");
        // Deep sleep test, trig D0 connected to RST after the delay
        // microsec
        // 1e6 = 1.000.000 => 1 sec !
        ESP.deepSleep(SLEEPDURATION * 1e3);
#else
        delay(SLEEPDURATION);
#endif
}

void watering(bool action){
   if (action) {
     // watering
     digitalWrite(LEDPIN, LOW); 
     digitalWrite(ELECTROPIN, HIGH);  
     }
   else
     {
     // Stop
     digitalWrite(LEDPIN, HIGH); 
     digitalWrite(ELECTROPIN, LOW);     
     }
  
}

// ********* Set cycle delays
void SetModeDelays() {
   if (ModeDuree ==0) {
      TimeUp = 1 * DEMOMODE;
      TimeStop = 3000 * DEMOMODE;
    }   
   if (ModeDuree ==1) {
      TimeUp = 1000 * DEMOMODE;
      TimeStop = 3000 * DEMOMODE;
    } 
   if (ModeDuree ==2) {
      TimeUp = 2000 * DEMOMODE;
      TimeStop = 2000 * DEMOMODE;
    }   
   if (ModeDuree ==3) {
      TimeUp = 3000 * DEMOMODE;
      TimeStop = 1000 * DEMOMODE;
    }   
   if (ModeDuree ==4) {
      TimeUp = 3000 * DEMOMODE;
      TimeStop = 1 * DEMOMODE;
    }   
}


   
// Check whether write to EEPROM was successful or not with the EEPROM.commit() function.
void eeprom_commit() {
  if (EEPROM.commit()) {
    Serial.println("EEPROM successfully committed!"); blink(LEDPIN);
  } else {
    Serial.println("ERROR! EEPROM commit failed!"); blink(LEDPIN, 6);
  }
}
