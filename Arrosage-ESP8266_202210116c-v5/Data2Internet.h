
#pragma once


/* ************* NODEMCU config ************ */
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
// Added to disable https fingerprint check 
#include <WiFiClientSecureBearSSL.h>

// For Https + Multi SSID
ESP8266WiFiMulti WiFiMulti;
wl_status_t LastWifiState = WL_IDLE_STATUS;
WiFiClient client;
/*
    Wifi P01, DEVICE DOMO 34
    Wifi P02, DEVICE DOMO 35
*/

bool alreadySent = false;
    
// Build the http query
char *append_str(char *here, String s) {  int i=0; while (*here++ = s[i]){i++;};return here-1;}
char *append_ul(char *here, unsigned long u) { char buf[20]; return append_str(here, ultoa(u, buf, 10));} 
char post_rqst[256];
char *p;
char *content_length_here;
char *json_start;
int compi;

// LED_BUILTIN = 2 for LoLin NodeMCU
#define LED_BUILTIN 2

/* ************* End of NODEMCU config ************ */

// NodeMCU local
// domoticz mesnilab:
char websiteIP[] = "192.168.1.100";
const int websitePORT = 8080;

// NodeMCU Internet
// char websiteIP[] = "my-domain.com";
//const int websitePORT = my-port;

bool init_internet(){

   bool result = false;
   // ---------------- Init network ----------------------------
   //WiFi.disconnect(); // No need. May cause crash ?
   Serial.println("Start WIFI_STA");
   delay(2000);


   // none = WiFi.setPhyMode(PHY_MODE_11G);
   // system_phy_set_max_tpw(powervalue max 82); divide by 4 for dBm value
   WiFi.mode(WIFI_STA);
   WiFiMulti.addAP("my-wifi",  "my-wifi-pwd");  // mesnilab

   // Wifi takes time
   delay(5000);

   // ---------------- Try Neywork (probably useless) ------------------------
   // wait for WiFi connection. Anyway, test fails !!
   LastWifiState = WiFiMulti.run();
   if (LastWifiState == WL_CONNECTED)
      {
      Serial.print("Wifi connected:"); Serial.print(WiFi.localIP());
      Serial.print(" SSID:"); Serial.println(WiFi.SSID());
//      getMyIP();
//      blink(2);
       //First query sometimes fails, so try it right now.
       {
        HTTPClient http;
        http.begin(client, "http://www.free.fr");
       }
       result = true;
      }
   else
      {
       Serial.println("Wifi connection fail, need more time");
//       blink(2,6,600);
       result = false;
      }
   return(result);
  
}

void sendMeasure (byte Devicetype, int Measure) {
 
    // replacing in URL the devicetype and value
    //request.replace("%DEVICE", Devicetype);
    //request.replace("%VALUE", Measure);

  if (!alreadySent) {
    // wait for WiFi connection
    if ((WiFiMulti.run() == WL_CONNECTED))
      {
      
      HTTPClient http;

      p = post_rqst;
      p = append_str(p, "http://" );
      p = append_str(p, websiteIP );
      p = append_str(p, ":" );
      p = append_str(p, String(websitePORT));
      p = append_str(p, "/json.htm?type=command&param=");
       p = append_str(p, F("udevice&idx="));
       p = append_str(p, String(Devicetype));
       p = append_str(p, F("&nvalue="));
       p = append_str(p, String(Measure));
                       
      // http.begin(* client, post_rqst);  // HTTPS 
      http.begin(client, post_rqst);  // HTTP 

      //Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();
  
      // httpCode will be negative on error
      if (httpCode > 0) 
        {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);
        // Was a success
        //lastreceivedtime = millis();
  
        // file found at server
        if ((httpCode == HTTP_CODE_OK) || (httpCode == HTTP_CODE_CREATED))
            {
            String payload = http.getString();
            Serial.println(payload);
            }
        } 
      else
        {
        Serial.printf("[HTTP] GET... failed, error: %d %s\n", httpCode, http.errorToString(httpCode).c_str());
        }

      // if (DEBUG) {Serial.printf("[HTTP] GET: %s\n",post_rqst);}
      http.end();
      }


// Serial.print(F(" Sent ")); Serial.println(post_rqst);
// end of NODEMCU

   /*
  if (DEBUG) {
       Serial.print(F(" Sent ")); Serial.println(phase);
       Serial.print(F(" Sent ")); Serial.println(post_rqst);
       //Serial.println(request); Serial.flush();
        }
    */
   alreadySent = true;

}
}
