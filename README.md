# Arrosage-hatlab

This is the code for the project detailed in Hatlab wikifab page : [Plant Watering](https://wikifab.hatlab.fr/Atelier_pour_Arrosage_automatique_et_autonome)

You can find all infos and resources of the project in the wikifab page.

## Getting started

Tested on D1 mini.

1. If you want to send measures to your **Domoticz** server:
- in **Arrosage-ESP8266_202210116c-vx.ino** file, uncomment _#define DATA2INTERNET_
- and set the proper Device Type in line _#define INTERNETDEVICE_
-in **Data2Internet.h** file, set the Domoticz server IP/name, and the wifi infos.

2. Upload code to your ESP8266. 

1. Upload web pages in "data" directory thanks to LittleFS. See how here: [LittleFS Introduction](https://microcontrollerslab.com/littlefs-introduction-install-esp8266-nodemcu-filesystem-uploader-arduino/)


